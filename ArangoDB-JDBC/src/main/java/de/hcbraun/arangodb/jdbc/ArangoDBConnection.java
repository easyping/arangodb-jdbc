/*
 * Copyright (C) 2015 Hans-Christian Braun
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.hcbraun.arangodb.jdbc;

import java.sql.Array;
import java.sql.Blob;
import java.sql.CallableStatement;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.NClob;
import java.sql.PreparedStatement;
import java.sql.SQLClientInfoException;
import java.sql.SQLException;
import java.sql.SQLWarning;
import java.sql.SQLXML;
import java.sql.Savepoint;
import java.sql.Statement;
import java.sql.Struct;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.Executor;

import org.apache.log4j.Logger;

import com.arangodb.ArangoConfigure;
import com.arangodb.ArangoDriver;
import com.arangodb.ArangoHost;
import com.arangodb.entity.EntityFactory;

public class ArangoDBConnection implements Connection {

	private final Logger logger = Logger.getLogger(ArangoDBConnection.class);
	
	private ArangoConfigure configure = new ArangoConfigure();
	private ArangoDriver driver = null;
	
	protected ArangoDBConnection(String host, String port, String database) {
		// Initialize configure
		configure.setArangoHost(new ArangoHost(host, Integer.parseInt(port)));
		configure.setDefaultDatabase(database);
		configure.init();
		
		// Create Driver (this instance is thread-safe)
		driver = new ArangoDriver(configure);
	}
	
	@Override
	public <T> T unwrap(Class<T> iface) throws SQLException {
		// TODO Auto-generated method stub
		logger.debug("Connection - unwarp");
		return null;
	}

	@Override
	public boolean isWrapperFor(Class<?> iface) throws SQLException {
		// TODO Auto-generated method stub
		logger.debug("Connection - isWrapperFor");
		return false;
	}

	@Override
	public Statement createStatement() throws SQLException {
		logger.debug("createStatement");
		return new ArangoDBStatement(driver);
	}

	/*
	 * Der Abfragebefehl darf Kommentar beinhalten, in dem die Spalten genauer beschrieben werden
	 * um im JasperReport entsprechend JDBC like darauf zuzugreifen.
	 * 
	 *  cols: <Feld-Name>:<Daten-Typ>,
	 *  
	 *  Feld-Name, wie das Feld innerhalb der JSON-Struktur angesprochen wird
	 *  Daten-Typ: (lang / kurz)
	 *  string / s
	 *  integer / i
	 *  boolean / b
	 *  double / d
	 *  float / f
	 *  bigdecimal / bd
	 *  date / dt
	 *  time / t
	 *  timestamp / ts
	 *  
	 * @see java.sql.Connection#prepareStatement(java.lang.String)
	 */
	@Override
	public PreparedStatement prepareStatement(String sql) throws SQLException {
		logger.debug("prepareStatement " + sql);
		return new ArangoDBPreparedStatement(driver, sql);
	}

	@Override
	public CallableStatement prepareCall(String sql) throws SQLException {
		// TODO Auto-generated method stub
		logger.debug("Connection - prepareCall");
		return null;
	}

	@Override
	public String nativeSQL(String sql) throws SQLException {
		// TODO Auto-generated method stub
		logger.debug("Connection - nativeSQL");
		return null;
	}

	@Override
	public void setAutoCommit(boolean autoCommit) throws SQLException {
		// TODO Auto-generated method stub
		logger.debug("Connection - setAutoCommit");
	}

	@Override
	public boolean getAutoCommit() throws SQLException {
		// TODO Auto-generated method stub
		logger.debug("Connection - getAutoCommit");
		return false;
	}

	@Override
	public void commit() throws SQLException {
		// TODO Auto-generated method stub
		logger.debug("Connection - commit");
	}

	@Override
	public void rollback() throws SQLException {
		// TODO Auto-generated method stub
		logger.debug("Connection - rollback");
	}

	@Override
	public void close() throws SQLException {
		driver = null;
	}

	@Override
	public boolean isClosed() throws SQLException {
		return driver == null;
	}

	@Override
	public DatabaseMetaData getMetaData() throws SQLException {
		// TODO Auto-generated method stub
		logger.debug("Connection - getMetaData");
		return null;
	}

	@Override
	public void setReadOnly(boolean readOnly) throws SQLException {
		// TODO Auto-generated method stub
		logger.debug("Connection - setReadOnly");
	}

	@Override
	public boolean isReadOnly() throws SQLException {
		// TODO Auto-generated method stub
		logger.debug("Connection - isReadOnly");
		return false;
	}

	@Override
	public void setCatalog(String catalog) throws SQLException {
		// TODO Auto-generated method stub
		logger.debug("Connection - setCatalog");
	}

	@Override
	public String getCatalog() throws SQLException {
		// TODO Auto-generated method stub
		logger.debug("Connection - getCatalog");
		return null;
	}

	@Override
	public void setTransactionIsolation(int level) throws SQLException {
		// TODO Auto-generated method stub
		logger.debug("Connection - setTransactionIsolation");
	}

	@Override
	public int getTransactionIsolation() throws SQLException {
		// TODO Auto-generated method stub
		logger.debug("Connection - getTransationIsolation");
		return 0;
	}

	@Override
	public SQLWarning getWarnings() throws SQLException {
		// TODO Auto-generated method stub
		logger.debug("Connection - getWarnings");
		return null;
	}

	@Override
	public void clearWarnings() throws SQLException {
		// TODO Auto-generated method stub
		logger.debug("Connection - clearWarings");
	}

	@Override
	public Statement createStatement(int resultSetType, int resultSetConcurrency)
			throws SQLException {
		// TODO Auto-generated method stub
		logger.debug("Connection - createStatement-1");
		return null;
	}

	@Override
	public PreparedStatement prepareStatement(String sql, int resultSetType,
			int resultSetConcurrency) throws SQLException {
		// TODO Auto-generated method stub
		logger.debug("Connection - prepareStatement-1");
		return null;
	}

	@Override
	public CallableStatement prepareCall(String sql, int resultSetType,
			int resultSetConcurrency) throws SQLException {
		// TODO Auto-generated method stub
		logger.debug("Connection - prepareCall-1");
		return null;
	}

	@Override
	public Map<String, Class<?>> getTypeMap() throws SQLException {
		// TODO Auto-generated method stub
		logger.debug("Connection - getTypeMap");
		return null;
	}

	@Override
	public void setTypeMap(Map<String, Class<?>> map) throws SQLException {
		// TODO Auto-generated method stub
		logger.debug("Connection - setTypeMap");
	}

	@Override
	public void setHoldability(int holdability) throws SQLException {
		// TODO Auto-generated method stub
		logger.debug("Connection - setHoldability");
	}

	@Override
	public int getHoldability() throws SQLException {
		// TODO Auto-generated method stub
		logger.debug("Connection - getHoldability");
		return 0;
	}

	@Override
	public Savepoint setSavepoint() throws SQLException {
		// TODO Auto-generated method stub
		logger.debug("Connection - setSavepoint");
		return null;
	}

	@Override
	public Savepoint setSavepoint(String name) throws SQLException {
		// TODO Auto-generated method stub
		logger.debug("Connection - setSavepoint-1");
		return null;
	}

	@Override
	public void rollback(Savepoint savepoint) throws SQLException {
		// TODO Auto-generated method stub
		logger.debug("Connection - rollback-1");
	}

	@Override
	public void releaseSavepoint(Savepoint savepoint) throws SQLException {
		// TODO Auto-generated method stub
		logger.debug("Connection - releaseSavepoint");
	}

	@Override
	public Statement createStatement(int resultSetType, int resultSetConcurrency,
			int resultSetHoldability) throws SQLException {
		// TODO Auto-generated method stub
		logger.debug("Connection - createStatement-2");
		return null;
	}

	@Override
	public PreparedStatement prepareStatement(String sql, int resultSetType,
			int resultSetConcurrency, int resultSetHoldability) throws SQLException {
		// TODO Auto-generated method stub
		logger.debug("Connection - prepareStatement-2");
		return null;
	}

	@Override
	public CallableStatement prepareCall(String sql, int resultSetType,
			int resultSetConcurrency, int resultSetHoldability) throws SQLException {
		// TODO Auto-generated method stub
		logger.debug("Connection - prepareCall-2");
		return null;
	}

	@Override
	public PreparedStatement prepareStatement(String sql, int autoGeneratedKeys)
			throws SQLException {
		// TODO Auto-generated method stub
		logger.debug("Connection - prepareStatement-3");
		return null;
	}

	@Override
	public PreparedStatement prepareStatement(String sql, int[] columnIndexes)
			throws SQLException {
		// TODO Auto-generated method stub
		logger.debug("Connection - prepareStatement-4");
		return null;
	}

	@Override
	public PreparedStatement prepareStatement(String sql, String[] columnNames)
			throws SQLException {
		// TODO Auto-generated method stub
		logger.debug("Connection - prepareStatement-5");
		return null;
	}

	@Override
	public Clob createClob() throws SQLException {
		// TODO Auto-generated method stub
		logger.debug("Connection - createClob");
		return null;
	}

	@Override
	public Blob createBlob() throws SQLException {
		// TODO Auto-generated method stub
		logger.debug("Connection - createBlob");
		return null;
	}

	@Override
	public NClob createNClob() throws SQLException {
		// TODO Auto-generated method stub
		logger.debug("Connection - createNClob");
		return null;
	}

	@Override
	public SQLXML createSQLXML() throws SQLException {
		// TODO Auto-generated method stub
		logger.debug("Connection - createSQLXML");
		return null;
	}

	@Override
	public boolean isValid(int timeout) throws SQLException {
		// TODO Auto-generated method stub
		logger.debug("Connection - isValid");
		return false;
	}

	@Override
	public void setClientInfo(String name, String value)
			throws SQLClientInfoException {
		// TODO Auto-generated method stub
		logger.debug("Connection - setClientInfo");
	}

	@Override
	public void setClientInfo(Properties properties)
			throws SQLClientInfoException {
		// TODO Auto-generated method stub
		logger.debug("Connection - setClientInfo-1");
	}

	@Override
	public String getClientInfo(String name) throws SQLException {
		// TODO Auto-generated method stub
		logger.debug("Connection - getClientInfo");
		return null;
	}

	@Override
	public Properties getClientInfo() throws SQLException {
		// TODO Auto-generated method stub
		logger.debug("Connection - getClientInfo-1");
		return null;
	}

	@Override
	public Array createArrayOf(String typeName, Object[] elements)
			throws SQLException {
		// TODO Auto-generated method stub
		logger.debug("Connection - createArrayOf");
		return null;
	}

	@Override
	public Struct createStruct(String typeName, Object[] attributes)
			throws SQLException {
		// TODO Auto-generated method stub
		logger.debug("Connection - createStuct");
		return null;
	}

	@Override
	public void setSchema(String schema) throws SQLException {
		// TODO Auto-generated method stub
		logger.debug("Connection - setSchema");
	}

	@Override
	public String getSchema() throws SQLException {
		// TODO Auto-generated method stub
		logger.debug("Connection - getSchema");
		return null;
	}

	@Override
	public void abort(Executor executor) throws SQLException {
		// TODO Auto-generated method stub
		logger.debug("Connection - abort");
	}

	@Override
	public void setNetworkTimeout(Executor executor, int milliseconds)
			throws SQLException {
		// TODO Auto-generated method stub
		logger.debug("Connection - setNetworkTimeout");
	}

	@Override
	public int getNetworkTimeout() throws SQLException {
		// TODO Auto-generated method stub
		logger.debug("Connection - getNetworkTimeout");
		return 0;
	}
	static {
		EntityFactory.configure(EntityFactory.getGsonBuilder().registerTypeAdapter(
				JdbcDocument.class, new JdbcDocument.JdbcDocumentDeserializer()));
	}

}
