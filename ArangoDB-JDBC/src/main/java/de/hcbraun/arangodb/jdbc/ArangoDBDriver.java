/*
 * Copyright (C) 2015 Hans-Christian Braun
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.hcbraun.arangodb.jdbc;

import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverPropertyInfo;
import java.sql.SQLException;
import java.sql.SQLFeatureNotSupportedException;
import java.util.Properties;
import java.util.logging.Logger;

public class ArangoDBDriver implements Driver {

	public final static String URL_PREFIX = "jdbc:hcbraun:arangodb";
	
	@Override
	public Connection connect(String url, Properties info) throws SQLException {
		String[] urlPart = url.split("/");
		String[] hostInfo = urlPart[0].split(":");
		String host = "127.0.0.1", port = "8529";
		if(hostInfo.length > 3)
			host = hostInfo[3];
		if(hostInfo.length > 4)
			port = hostInfo[4];
		return new ArangoDBConnection(host, port, urlPart[1]);
	}

	@Override
	public boolean acceptsURL(String url) throws SQLException {
		return url.startsWith(URL_PREFIX) && url.indexOf("/") > 0;
	}

	@Override
	public DriverPropertyInfo[] getPropertyInfo(String url, Properties info)
			throws SQLException {
		return new DriverPropertyInfo[0];
	}

	@Override
	public int getMajorVersion() {
		return 0;
	}

	@Override
	public int getMinorVersion() {
		return 1;
	}

	@Override
	public boolean jdbcCompliant() {
		return false;
	}

	@Override
	public Logger getParentLogger() throws SQLFeatureNotSupportedException {
		throw new SQLFeatureNotSupportedException("methodNotSupported: Driver.getParentLogger()");
	}

	static
	{
		try
		{
			java.sql.DriverManager.registerDriver(new ArangoDBDriver());
		}
		catch (SQLException e)
		{
			throw new RuntimeException("ArangoDBDriver-Init-Error: " + e.getMessage());
		}
	}
}
