/*
 * Copyright (C) 2015 Hans-Christian Braun
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.hcbraun.arangodb.jdbc;

import java.io.InputStream;
import java.io.Reader;
import java.math.BigDecimal;
import java.net.URL;
import java.sql.Array;
import java.sql.Blob;
import java.sql.Clob;
import java.sql.Date;
import java.sql.NClob;
import java.sql.ParameterMetaData;
import java.sql.PreparedStatement;
import java.sql.Ref;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.RowId;
import java.sql.SQLException;
import java.sql.SQLXML;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;

import com.arangodb.ArangoDriver;
import com.arangodb.ArangoException;
import com.arangodb.DocumentCursor;
import com.arangodb.entity.BaseDocument;
import com.arangodb.util.MapBuilder;

public class ArangoDBPreparedStatement extends ArangoDBStatement implements
		PreparedStatement {

	private final Logger logger = Logger
			.getLogger(ArangoDBPreparedStatement.class);

	private String sql = null;
	private ArrayList<String> para = new ArrayList<String>();
	private MapBuilder mapBuild = new MapBuilder();

	protected ArangoDBPreparedStatement(ArangoDriver driver, String sql) {
		super(driver);
		Pattern pattern = Pattern.compile("@[\\w]+");
		Matcher matcher = pattern.matcher(sql);
		while (matcher.find())
			para.add(matcher.group().substring(1));

		if (sql.indexOf("?") >= 0) {
			String[] part = sql.split("\\?");
			StringBuffer nsql = new StringBuffer();
			int p = 1;
			for (int i = 0; i < part.length - 1; i++) {
				String s = part[i];
				nsql.append(s);
				s = "para" + p++;
				para.add(s);
				nsql.append("@");
				nsql.append(s);
			}
			nsql.append(part[part.length - 1]);
			sql = nsql.toString();
			logger.debug("Neu-Sql: " + sql);
		}
		this.sql = sql;
	}

	@Override
	public ResultSet executeQuery() throws SQLException {
		try {
			return new ArangoDBResultSet(driver.executeDocumentQuery(sql,
					mapBuild.get(), null, JdbcDocument.class), this, sql);
		} catch (ArangoException e) {
			e.printStackTrace();
			throw new SQLException(e.getMessage());
		}
	}

	@Override
	public int executeUpdate() throws SQLException {
		try {
			DocumentCursor<BaseDocument> rs = driver.executeDocumentQuery(sql,
					mapBuild.get(), null, BaseDocument.class);
			Iterator<BaseDocument> it = rs.entityIterator();
			int c = 0;
			while (it.hasNext()) {
				BaseDocument doc = it.next();
				if (doc.getDocumentKey() != null)
					c++;
			}
			return c;
		} catch (ArangoException e) {
			System.out.println("Failed to execute query. " + e.getMessage());
			throw new SQLException(e.getMessage());
		}
	}

	@Override
	public void setNull(int parameterIndex, int sqlType) throws SQLException {
		mapBuild.put(para.get(parameterIndex - 1), null);
	}

	@Override
	public void setBoolean(int parameterIndex, boolean x) throws SQLException {
		mapBuild.put(para.get(parameterIndex - 1), x ? "true" : "false");
	}

	@Override
	public void setByte(int parameterIndex, byte x) throws SQLException {
		mapBuild.put(para.get(parameterIndex - 1), x);
	}

	@Override
	public void setShort(int parameterIndex, short x) throws SQLException {
		mapBuild.put(para.get(parameterIndex - 1), x);
	}

	@Override
	public void setInt(int parameterIndex, int x) throws SQLException {
		mapBuild.put(para.get(parameterIndex - 1), x);
	}

	@Override
	public void setLong(int parameterIndex, long x) throws SQLException {
		mapBuild.put(para.get(parameterIndex - 1), x);
	}

	@Override
	public void setFloat(int parameterIndex, float x) throws SQLException {
		mapBuild.put(para.get(parameterIndex - 1), x);
	}

	@Override
	public void setDouble(int parameterIndex, double x) throws SQLException {
		mapBuild.put(para.get(parameterIndex - 1), x);
	}

	@Override
	public void setBigDecimal(int parameterIndex, BigDecimal x)
			throws SQLException {
		mapBuild.put(para.get(parameterIndex - 1), x);
	}

	@Override
	public void setString(int parameterIndex, String x) throws SQLException {
		mapBuild.put(para.get(parameterIndex - 1), x);
	}

	@Override
	public void setBytes(int parameterIndex, byte[] x) throws SQLException {
		mapBuild.put(para.get(parameterIndex - 1), x);
	}

	@Override
	public void setDate(int parameterIndex, Date x) throws SQLException {
		mapBuild.put(para.get(parameterIndex - 1), x);
	}

	@Override
	public void setTime(int parameterIndex, Time x) throws SQLException {
		mapBuild.put(para.get(parameterIndex - 1), x);
	}

	@Override
	public void setTimestamp(int parameterIndex, Timestamp x) throws SQLException {
		mapBuild.put(para.get(parameterIndex - 1), x);
	}

	@Override
	public void setAsciiStream(int parameterIndex, InputStream x, int length)
			throws SQLException {
		// TODO Auto-generated method stub

	}

	@Override
	public void setUnicodeStream(int parameterIndex, InputStream x, int length)
			throws SQLException {
		// TODO Auto-generated method stub

	}

	@Override
	public void setBinaryStream(int parameterIndex, InputStream x, int length)
			throws SQLException {
		// TODO Auto-generated method stub

	}

	@Override
	public void clearParameters() throws SQLException {
		mapBuild = new MapBuilder();
	}

	@Override
	public void setObject(int parameterIndex, Object x, int targetSqlType)
			throws SQLException {
		mapBuild.put(para.get(parameterIndex - 1), x);
	}

	@Override
	public void setObject(int parameterIndex, Object x) throws SQLException {
		mapBuild.put(para.get(parameterIndex - 1), x);
	}

	@Override
	public boolean execute() throws SQLException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void addBatch() throws SQLException {
		// TODO Auto-generated method stub

	}

	@Override
	public void setCharacterStream(int parameterIndex, Reader reader, int length)
			throws SQLException {
		// TODO Auto-generated method stub

	}

	@Override
	public void setRef(int parameterIndex, Ref x) throws SQLException {
		// TODO Auto-generated method stub

	}

	@Override
	public void setBlob(int parameterIndex, Blob x) throws SQLException {
		// TODO Auto-generated method stub

	}

	@Override
	public void setClob(int parameterIndex, Clob x) throws SQLException {
		// TODO Auto-generated method stub

	}

	@Override
	public void setArray(int parameterIndex, Array x) throws SQLException {
		// TODO Auto-generated method stub

	}

	@Override
	public ResultSetMetaData getMetaData() throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setDate(int parameterIndex, Date x, Calendar cal)
			throws SQLException {
		// TODO Auto-generated method stub

	}

	@Override
	public void setTime(int parameterIndex, Time x, Calendar cal)
			throws SQLException {
		// TODO Auto-generated method stub

	}

	@Override
	public void setTimestamp(int parameterIndex, Timestamp x, Calendar cal)
			throws SQLException {
		// TODO Auto-generated method stub

	}

	@Override
	public void setNull(int parameterIndex, int sqlType, String typeName)
			throws SQLException {
		// TODO Auto-generated method stub

	}

	@Override
	public void setURL(int parameterIndex, URL x) throws SQLException {
		// TODO Auto-generated method stub

	}

	@Override
	public ParameterMetaData getParameterMetaData() throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setRowId(int parameterIndex, RowId x) throws SQLException {
		// TODO Auto-generated method stub

	}

	@Override
	public void setNString(int parameterIndex, String value) throws SQLException {
		// TODO Auto-generated method stub

	}

	@Override
	public void setNCharacterStream(int parameterIndex, Reader value, long length)
			throws SQLException {
		// TODO Auto-generated method stub

	}

	@Override
	public void setNClob(int parameterIndex, NClob value) throws SQLException {
		// TODO Auto-generated method stub

	}

	@Override
	public void setClob(int parameterIndex, Reader reader, long length)
			throws SQLException {
		// TODO Auto-generated method stub

	}

	@Override
	public void setBlob(int parameterIndex, InputStream inputStream, long length)
			throws SQLException {
		// TODO Auto-generated method stub

	}

	@Override
	public void setNClob(int parameterIndex, Reader reader, long length)
			throws SQLException {
		// TODO Auto-generated method stub

	}

	@Override
	public void setSQLXML(int parameterIndex, SQLXML xmlObject)
			throws SQLException {
		// TODO Auto-generated method stub

	}

	@Override
	public void setObject(int parameterIndex, Object x, int targetSqlType,
			int scaleOrLength) throws SQLException {
		// TODO Auto-generated method stub

	}

	@Override
	public void setAsciiStream(int parameterIndex, InputStream x, long length)
			throws SQLException {
		// TODO Auto-generated method stub

	}

	@Override
	public void setBinaryStream(int parameterIndex, InputStream x, long length)
			throws SQLException {
		// TODO Auto-generated method stub

	}

	@Override
	public void setCharacterStream(int parameterIndex, Reader reader, long length)
			throws SQLException {
		// TODO Auto-generated method stub

	}

	@Override
	public void setAsciiStream(int parameterIndex, InputStream x)
			throws SQLException {
		// TODO Auto-generated method stub

	}

	@Override
	public void setBinaryStream(int parameterIndex, InputStream x)
			throws SQLException {
		// TODO Auto-generated method stub

	}

	@Override
	public void setCharacterStream(int parameterIndex, Reader reader)
			throws SQLException {
		// TODO Auto-generated method stub

	}

	@Override
	public void setNCharacterStream(int parameterIndex, Reader value)
			throws SQLException {
		// TODO Auto-generated method stub

	}

	@Override
	public void setClob(int parameterIndex, Reader reader) throws SQLException {
		// TODO Auto-generated method stub

	}

	@Override
	public void setBlob(int parameterIndex, InputStream inputStream)
			throws SQLException {
		// TODO Auto-generated method stub

	}

	@Override
	public void setNClob(int parameterIndex, Reader reader) throws SQLException {
		// TODO Auto-generated method stub

	}

}
