/*
 * Copyright (C) 2015 Hans-Christian Braun
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.hcbraun.arangodb.jdbc;

import java.io.InputStream;
import java.io.Reader;
import java.math.BigDecimal;
import java.net.URL;
import java.sql.Array;
import java.sql.Blob;
import java.sql.Clob;
import java.sql.Date;
import java.sql.NClob;
import java.sql.Ref;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.RowId;
import java.sql.SQLException;
import java.sql.SQLWarning;
import java.sql.SQLXML;
import java.sql.Statement;
import java.sql.Time;
import java.sql.Timestamp;
import java.sql.Types;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.sql.RowSetMetaData;
import javax.sql.rowset.RowSetMetaDataImpl;

import org.apache.log4j.Logger;

import com.arangodb.ArangoException;
import com.arangodb.DocumentCursor;
import com.arangodb.entity.BaseDocument;

public class ArangoDBResultSet implements ResultSet {

	private final Logger logger = Logger.getLogger(ArangoDBResultSet.class);

	private final static Pattern commentsPattern = Pattern.compile(
			"(//.*?$)|(/\\*.*?\\*/)", Pattern.MULTILINE | Pattern.DOTALL);

	private final static String[] DTYP_SHORT = { "s", "i", "b", "d", "f", "bd",
			"dt", "t", "ts" };
	private final static String[] DTYP_LONG = { "string", "integer", "boolean",
			"double", "float", "bigdecimal", "date", "time", "timestamp" };
	private final static int[] DTYP_TYPES = { Types.VARCHAR, Types.INTEGER,
			Types.BOOLEAN, Types.DOUBLE, Types.FLOAT, Types.DECIMAL, Types.DATE,
			Types.TIME, Types.TIMESTAMP };

	private DocumentCursor<JdbcDocument> dc = null;
	private ArangoDBStatement stat = null;
	private Iterator<JdbcDocument> it = null;
	private BaseDocument doc = null;
	private boolean wasNull = false, first = false, last = false;
	private int row = 0;
	private ArrayList<ColInfo> lstCol = new ArrayList<ColInfo>();

	private DateFormat dfJs = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
	private DateFormat dfSD = new SimpleDateFormat("yyyy-MM-dd");
	private DateFormat dfST = new SimpleDateFormat("HH:mm:ss");
	private DateFormat dfTS = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

	private RowSetMetaData rsmd = null;

	private class ColInfo {
		String name;
		int type = 0;

		ColInfo(String name, int type) {
			this.name = name;
			this.type = type;
		}
	}

	protected ArangoDBResultSet(DocumentCursor<JdbcDocument> rs,
			ArangoDBStatement stat, String sql) {
		this.dc = rs;
		this.stat = stat;

		Matcher matcher = commentsPattern.matcher(sql);
		while (matcher.find()) {
			String comment = matcher.group();
			logger.debug("comment: " + comment);
			if (comment.indexOf("cols:") > 0) {
				logger.debug("cols");
				if (comment.trim().endsWith("*/"))
					comment = comment.trim().substring(0, comment.trim().length() - 2);
				String[] cols = comment.substring(comment.indexOf("cols:") + 5).split(
						",");
				for (String c : cols) {
					logger.debug(c);
					String[] nt = c.trim().split(":");
					int typ = -1, i = 0;
					String nam = nt[1].toLowerCase();
					for (String t : DTYP_SHORT) {
						if (t.equals(nam)) {
							typ = DTYP_TYPES[i];
							break;
						}
						i++;
					}
					if (typ == -1) {
						i = 0;
						for (String t : DTYP_LONG) {
							if (t.equals(nam)) {
								typ = DTYP_TYPES[i];
								break;
							}
							i++;
						}
						if (typ == -1)
							typ = Types.VARCHAR;
					}
					lstCol.add(new ColInfo(nt[0], typ));
				}
				rsmd = new RowSetMetaDataImpl();
				try {
					rsmd.setColumnCount(lstCol.size());
					int i = 1;
					for (ColInfo ci : lstCol) {
						rsmd.setColumnName(i, ci.name);
						rsmd.setColumnType(i, ci.type);
						i++;
					}
				} catch (SQLException e) {
					e.printStackTrace();
					rsmd = null;
				}
			}
		}
	}

	@Override
	public <T> T unwrap(Class<T> iface) throws SQLException {
		logger.debug("unwarp");
		return null;
	}

	@Override
	public boolean isWrapperFor(Class<?> iface) throws SQLException {
		logger.debug("isWrapperFor");
		return false;
	}

	@Override
	public boolean next() throws SQLException {
		if (it == null && dc != null) {
			it = dc.entityIterator();
			first = true;
			// Wenn vor dem ersten Aufruf von next schon getMetaData aufgerufen wurde
			if (doc != null) {
				row++;
				return true;
			}
		} else
			first = false;
		if (it == null || !it.hasNext()) {
			first = false;
			it = null;
			return false;
		}
		doc = it.next();
		logger.debug("next: " + doc.getDocumentKey());
		row++;
		last = !it.hasNext();
		return true;
	}

	@Override
	public void close() throws SQLException {
		it = null;
		try {
			if (dc.getCursorId() >= 0)
				dc.close();
		} catch (ArangoException e) {
			e.printStackTrace();
		}
		dc = null;
	}

	@Override
	public boolean wasNull() throws SQLException {
		return wasNull;
	}

	@Override
	public String getString(int columnIndex) throws SQLException {
		Object obj = getObject(columnIndex);
		return obj == null ? null : obj.toString();
	}

	@Override
	public boolean getBoolean(int columnIndex) throws SQLException {
		Object obj = getObject(columnIndex);
		if (obj instanceof Boolean)
			return ((Boolean) obj).booleanValue();
		return obj == null ? false : obj.toString().equalsIgnoreCase("true");
	}

	@Override
	public byte getByte(int columnIndex) throws SQLException {
		logger.debug("getByte");
		return 0;
	}

	@Override
	public short getShort(int columnIndex) throws SQLException {
		Object obj = getObject(columnIndex);
		if (obj instanceof Double)
			return ((Double) obj).shortValue();
		return obj == null ? 0 : Short.parseShort(obj.toString());
	}

	@Override
	public int getInt(int columnIndex) throws SQLException {
		Object obj = getObject(columnIndex);
		if (obj instanceof Double)
			return ((Double) obj).intValue();
		return obj == null ? 0 : Integer.parseInt(obj.toString());
	}

	@Override
	public long getLong(int columnIndex) throws SQLException {
		Object obj = getObject(columnIndex);
		if (obj instanceof Double)
			return ((Double) obj).longValue();
		return obj == null ? 0 : Long.parseLong(obj.toString());
	}

	@Override
	public float getFloat(int columnIndex) throws SQLException {
		Object obj = getObject(columnIndex);
		if (obj instanceof Double)
			return ((Double) obj).floatValue();
		return obj == null ? 0 : Float.parseFloat(obj.toString());
	}

	@Override
	public double getDouble(int columnIndex) throws SQLException {
		Object obj = getObject(columnIndex);
		if (obj instanceof Double)
			return ((Double) obj).doubleValue();
		return obj == null ? 0 : Double.parseDouble(obj.toString());
	}

	@Override
	public BigDecimal getBigDecimal(int columnIndex, int scale)
			throws SQLException {
		Object obj = getObject(columnIndex);
		if (obj instanceof Double)
			return new BigDecimal(((Double) obj).doubleValue()).setScale(scale);
		return obj == null ? null : new BigDecimal(obj.toString()).setScale(scale);
	}

	@Override
	public byte[] getBytes(int columnIndex) throws SQLException {
		logger.debug("getBytes");
		return null;
	}

	@Override
	public Date getDate(int columnIndex) throws SQLException {
		Object obj = getObject(columnIndex);
		if (obj != null) {
			String s = obj.toString();
			Date d = null;
			if (s.indexOf("T") > 0) {
				try {
					s = s.replace('T', ' ');
					d = new Date(dfJs.parse(s).getTime());
				} catch (ParseException e) {
					e.printStackTrace();
				}
			} else {
				try {
					d = new Date(dfSD.parse(s).getTime());
				} catch (ParseException e) {
					e.printStackTrace();
				}
			}
			return d;
		}
		return null;
	}

	@Override
	public Time getTime(int columnIndex) throws SQLException {
		Object obj = getObject(columnIndex);
		if (obj != null) {
			String s = obj.toString();
			Time d = null;
			if (s.indexOf("T") > 0) {
				try {
					s = s.replace('T', ' ');
					d = new Time(dfJs.parse(s).getTime());
				} catch (ParseException e) {
					e.printStackTrace();
				}
			} else {
				try {
					d = new Time(dfST.parse(s).getTime());
				} catch (ParseException e) {
					e.printStackTrace();
				}
			}
			return d;
		}
		return null;
	}

	@Override
	public Timestamp getTimestamp(int columnIndex) throws SQLException {
		Object obj = getObject(columnIndex);
		if (obj != null) {
			String s = obj.toString();
			Timestamp d = null;
			if (s.indexOf("T") > 0) {
				try {
					s = s.replace('T', ' ');
					d = new Timestamp(dfJs.parse(s).getTime());
				} catch (ParseException e) {
					e.printStackTrace();
				}
			} else {
				try {
					d = new Timestamp(dfTS.parse(s).getTime());
				} catch (ParseException e) {
					e.printStackTrace();
				}
			}
			return d;
		}
		return null;
	}

	@Override
	public InputStream getAsciiStream(int columnIndex) throws SQLException {
		logger.debug("getAsciiStream");
		return null;
	}

	@Override
	public InputStream getUnicodeStream(int columnIndex) throws SQLException {
		logger.debug("getUnicodeStream");
		return null;
	}

	@Override
	public InputStream getBinaryStream(int columnIndex) throws SQLException {
		logger.debug("getBinaryStream");
		return null;
	}

	@Override
	public String getString(String columnLabel) throws SQLException {
		Object obj = getObject(columnLabel);
		return obj == null ? null : obj.toString();
	}

	@Override
	public boolean getBoolean(String columnLabel) throws SQLException {
		Object obj = getObject(columnLabel);
		if (obj instanceof Boolean)
			return ((Boolean) obj).booleanValue();
		return obj == null ? false : obj.toString().equalsIgnoreCase("true");
	}

	@Override
	public byte getByte(String columnLabel) throws SQLException {
		logger.debug("getByte");
		return 0;
	}

	@Override
	public short getShort(String columnLabel) throws SQLException {
		Object obj = getObject(columnLabel);
		if (obj instanceof Double)
			return ((Double) obj).shortValue();
		return obj == null ? 0 : Short.parseShort(obj.toString());
	}

	@Override
	public int getInt(String columnLabel) throws SQLException {
		Object obj = getObject(columnLabel);
		if (obj instanceof Double)
			return ((Double) obj).intValue();
		return obj == null ? 0 : Integer.parseInt(obj.toString());
	}

	@Override
	public long getLong(String columnLabel) throws SQLException {
		Object obj = getObject(columnLabel);
		if (obj instanceof Double)
			return ((Double) obj).longValue();
		return obj == null ? 0 : Long.parseLong(obj.toString());
	}

	@Override
	public float getFloat(String columnLabel) throws SQLException {
		Object obj = getObject(columnLabel);
		if (obj instanceof Double)
			return ((Double) obj).floatValue();
		return obj == null ? 0 : Float.parseFloat(obj.toString());
	}

	@Override
	public double getDouble(String columnLabel) throws SQLException {
		Object obj = getObject(columnLabel);
		if (obj instanceof Double)
			return ((Double) obj).doubleValue();
		return obj == null ? 0 : Double.parseDouble(obj.toString());
	}

	@Override
	public BigDecimal getBigDecimal(String columnLabel, int scale)
			throws SQLException {
		Object obj = getObject(columnLabel);
		if (obj instanceof Double)
			return new BigDecimal(((Double) obj).doubleValue()).setScale(scale);
		return obj == null ? null : new BigDecimal(obj.toString()).setScale(scale);
	}

	@Override
	public byte[] getBytes(String columnLabel) throws SQLException {
		logger.debug("getBytes");
		return null;
	}

	@Override
	public Date getDate(String columnLabel) throws SQLException {
		Object obj = getObject(columnLabel);
		if (obj != null) {
			String s = obj.toString();
			Date d = null;
			if (s.indexOf("T") > 0) {
				try {
					s = s.replace('T', ' ');
					d = new Date(dfJs.parse(s).getTime());
				} catch (ParseException e) {
					e.printStackTrace();
				}
			} else {
				try {
					d = new Date(dfSD.parse(s).getTime());
				} catch (ParseException e) {
					e.printStackTrace();
				}
			}
			return d;
		}
		return null;
	}

	@Override
	public Time getTime(String columnLabel) throws SQLException {
		Object obj = getObject(columnLabel);
		if (obj != null) {
			String s = obj.toString();
			Time d = null;
			if (s.indexOf("T") > 0) {
				try {
					s = s.replace('T', ' ');
					d = new Time(dfJs.parse(s).getTime());
				} catch (ParseException e) {
					e.printStackTrace();
				}
			} else {
				try {
					d = new Time(dfST.parse(s).getTime());
				} catch (ParseException e) {
					e.printStackTrace();
				}
			}
			return d;
		}
		return null;
	}

	@Override
	public Timestamp getTimestamp(String columnLabel) throws SQLException {
		Object obj = getObject(columnLabel);
		if (obj != null) {
			String s = obj.toString();
			Timestamp d = null;
			if (s.indexOf("T") > 0) {
				try {
					s = s.replace('T', ' ');
					d = new Timestamp(dfJs.parse(s).getTime());
				} catch (ParseException e) {
					e.printStackTrace();
				}
			} else {
				try {
					d = new Timestamp(dfTS.parse(s).getTime());
				} catch (ParseException e) {
					e.printStackTrace();
				}
			}
			return d;
		}
		return null;
	}

	@Override
	public InputStream getAsciiStream(String columnLabel) throws SQLException {
		logger.debug("getAsciiStream");
		return null;
	}

	@Override
	public InputStream getUnicodeStream(String columnLabel) throws SQLException {
		logger.debug("getUnicodeStream");
		return null;
	}

	@Override
	public InputStream getBinaryStream(String columnLabel) throws SQLException {
		logger.debug("getBinaryStream");
		return null;
	}

	@Override
	public SQLWarning getWarnings() throws SQLException {
		logger.debug("getWarinngs");
		return null;
	}

	@Override
	public void clearWarnings() throws SQLException {
		logger.debug("clearWarnings");
	}

	@Override
	public String getCursorName() throws SQLException {
		return dc.getCursorId().toString();
	}

	@Override
	public ResultSetMetaData getMetaData() throws SQLException {
		if (rsmd != null)
			return rsmd;
		logger.debug("getMetaData");
		Iterator<JdbcDocument> d = dc.entityIterator();
		if (doc != null || d.hasNext()) {
			lstCol.clear();
			BaseDocument bd = doc != null ? doc : (doc = d.next());
			if (doc.getDocumentKey() != null) {
				lstCol.add(new ColInfo("_key", Types.VARCHAR));
				logger.debug(bd.getDocumentKey());
			}
			createColModel(bd.getProperties(), "");
			int i = 1;
			rsmd = new RowSetMetaDataImpl();
			rsmd.setColumnCount(lstCol.size());
			for (ColInfo ci : lstCol) {
				rsmd.setColumnName(i, ci.name);
				rsmd.setColumnType(i, ci.type);
				i++;
			}
			return rsmd;
		}
		return null;
	}

	private void createColModel(Map<String, Object> lst, String prefix) {
		for (String k : lst.keySet()) {
			Object data = lst.get(k);
			logger.debug(k + ": " + data.getClass().getName());
			if (data instanceof String)
				lstCol.add(new ColInfo(prefix + k, Types.VARCHAR));
			else if (data instanceof Map)
				createColModel((Map<String, Object>) data, prefix + k + ".");
			else
				lstCol.add(new ColInfo(prefix + k, Types.VARCHAR));
		}
	}

	@Override
	public Object getObject(int columnIndex) throws SQLException {
		Object obj = null;
		ColInfo cn = columnIndex - 1 < lstCol.size() ? lstCol.get(columnIndex - 1)
				: null;
		if (cn != null)
			obj = getObject(cn.name);
		wasNull = obj == null;
		return obj;
	}

	@Override
	public Object getObject(String columnLabel) throws SQLException {
		if (columnLabel.indexOf(".") > 0) {
			Object obj = doc.getAttribute(getColName(columnLabel.substring(0,
					columnLabel.indexOf("."))));
			if (obj != null) {
				if (obj instanceof Map) {
					return getObjData(
							columnLabel.substring(columnLabel.indexOf(".") + 1),
							(Map<String, Object>) obj);
				} else if (obj instanceof List) {
					String cn = columnLabel.substring(0, columnLabel.indexOf("."));
					int pos = 0;
					if (cn.endsWith("]"))
						pos = Integer.parseInt(cn.substring(cn.indexOf("[") + 1,
								cn.length() - 1));
					return getObjData(
							columnLabel.substring(columnLabel.indexOf(".") + 1),
							(Map<String, Object>) ((List) obj).get(pos));
				}
				return obj;
			}
			return null;
		}
		Object obj = doc.getAttribute(columnLabel);
		wasNull = obj == null;
		return obj;
	}

	private String getColName(String col) {
		if(col.indexOf("[") > 0)
			return col.substring(0, col.indexOf("["));
		return col;
	}
	
	private Object getObjData(String col, Map<String, Object> data) {
		Object obj = null;
		if (col.indexOf(".") > 0)
			obj = data.get(getColName(col.substring(0, col.indexOf("."))));
		else
			obj = data.get(col);
		if (obj instanceof Map)
			return getObjData(col.substring(col.indexOf(".") + 1),
					(Map<String, Object>) obj);
		else if (obj instanceof List) {
			String cn = col.substring(0, col.indexOf("."));
			int pos = 0;
			if (cn.endsWith("]"))
				pos = Integer.parseInt(cn.substring(cn.indexOf("[") + 1,
						cn.length() - 1));
			return getObjData(col.substring(col.indexOf(".") + 1),
					(Map<String, Object>) ((List) obj).get(pos));
		}
		return obj;
	}

	@Override
	public int findColumn(String columnLabel) throws SQLException {
		if (rsmd != null) {
			int p = 1;
			for (ColInfo ci : lstCol)
				if (ci.name.equals(columnLabel))
					break;
				else
					p++;
			return p;
		}
		lstCol.add(new ColInfo(columnLabel, 0));
		return lstCol.size();
	}

	@Override
	public Reader getCharacterStream(int columnIndex) throws SQLException {
		logger.debug("getCharacterStream");
		return null;
	}

	@Override
	public Reader getCharacterStream(String columnLabel) throws SQLException {
		logger.debug("getCharacterStream");
		return null;
	}

	@Override
	public BigDecimal getBigDecimal(int columnIndex) throws SQLException {
		Object obj = getObject(columnIndex);
		if (obj instanceof Double)
			return new BigDecimal(((Double) obj).doubleValue());
		return obj == null ? null : new BigDecimal(obj.toString());
	}

	@Override
	public BigDecimal getBigDecimal(String columnLabel) throws SQLException {
		Object obj = getObject(columnLabel);
		if (obj instanceof Double)
			return new BigDecimal(((Double) obj).doubleValue());
		return obj == null ? null : new BigDecimal(obj.toString());
	}

	@Override
	public boolean isBeforeFirst() throws SQLException {
		return it == null && dc != null;
	}

	@Override
	public boolean isAfterLast() throws SQLException {
		return it == null && last;
	}

	@Override
	public boolean isFirst() throws SQLException {
		return first;
	}

	@Override
	public boolean isLast() throws SQLException {
		return last;
	}

	@Override
	public void beforeFirst() throws SQLException {
		logger.debug("beforeFirst");
	}

	@Override
	public void afterLast() throws SQLException {
		logger.debug("afterLast");
	}

	@Override
	public boolean first() throws SQLException {
		return first;
	}

	@Override
	public boolean last() throws SQLException {
		return last;
	}

	@Override
	public int getRow() throws SQLException {
		return row;
	}

	@Override
	public boolean absolute(int row) throws SQLException {
		logger.debug("absolute");
		return false;
	}

	@Override
	public boolean relative(int rows) throws SQLException {
		logger.debug("relative");
		return false;
	}

	@Override
	public boolean previous() throws SQLException {
		logger.debug("previous");
		return false;
	}

	@Override
	public void setFetchDirection(int direction) throws SQLException {
		logger.debug("setFetchDirection");
	}

	@Override
	public int getFetchDirection() throws SQLException {
		logger.debug("getFetchDirection");
		return 0;
	}

	@Override
	public void setFetchSize(int rows) throws SQLException {
		logger.debug("setFetchSize");
	}

	@Override
	public int getFetchSize() throws SQLException {
		return 1;
	}

	@Override
	public int getType() throws SQLException {
		return ResultSet.TYPE_FORWARD_ONLY;
	}

	@Override
	public int getConcurrency() throws SQLException {
		return ResultSet.CONCUR_READ_ONLY;
	}

	@Override
	public boolean rowUpdated() throws SQLException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean rowInserted() throws SQLException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean rowDeleted() throws SQLException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void updateNull(int columnIndex) throws SQLException {
		// TODO Auto-generated method stub

	}

	@Override
	public void updateBoolean(int columnIndex, boolean x) throws SQLException {
		// TODO Auto-generated method stub

	}

	@Override
	public void updateByte(int columnIndex, byte x) throws SQLException {
		// TODO Auto-generated method stub

	}

	@Override
	public void updateShort(int columnIndex, short x) throws SQLException {
		// TODO Auto-generated method stub

	}

	@Override
	public void updateInt(int columnIndex, int x) throws SQLException {
		// TODO Auto-generated method stub

	}

	@Override
	public void updateLong(int columnIndex, long x) throws SQLException {
		// TODO Auto-generated method stub

	}

	@Override
	public void updateFloat(int columnIndex, float x) throws SQLException {
		// TODO Auto-generated method stub

	}

	@Override
	public void updateDouble(int columnIndex, double x) throws SQLException {
		// TODO Auto-generated method stub

	}

	@Override
	public void updateBigDecimal(int columnIndex, BigDecimal x)
			throws SQLException {
		// TODO Auto-generated method stub

	}

	@Override
	public void updateString(int columnIndex, String x) throws SQLException {
		// TODO Auto-generated method stub

	}

	@Override
	public void updateBytes(int columnIndex, byte[] x) throws SQLException {
		// TODO Auto-generated method stub

	}

	@Override
	public void updateDate(int columnIndex, Date x) throws SQLException {
		// TODO Auto-generated method stub

	}

	@Override
	public void updateTime(int columnIndex, Time x) throws SQLException {
		// TODO Auto-generated method stub

	}

	@Override
	public void updateTimestamp(int columnIndex, Timestamp x) throws SQLException {
		// TODO Auto-generated method stub

	}

	@Override
	public void updateAsciiStream(int columnIndex, InputStream x, int length)
			throws SQLException {
		// TODO Auto-generated method stub

	}

	@Override
	public void updateBinaryStream(int columnIndex, InputStream x, int length)
			throws SQLException {
		// TODO Auto-generated method stub

	}

	@Override
	public void updateCharacterStream(int columnIndex, Reader x, int length)
			throws SQLException {
		// TODO Auto-generated method stub

	}

	@Override
	public void updateObject(int columnIndex, Object x, int scaleOrLength)
			throws SQLException {
		// TODO Auto-generated method stub

	}

	@Override
	public void updateObject(int columnIndex, Object x) throws SQLException {
		// TODO Auto-generated method stub

	}

	@Override
	public void updateNull(String columnLabel) throws SQLException {
		// TODO Auto-generated method stub

	}

	@Override
	public void updateBoolean(String columnLabel, boolean x) throws SQLException {
		// TODO Auto-generated method stub

	}

	@Override
	public void updateByte(String columnLabel, byte x) throws SQLException {
		// TODO Auto-generated method stub

	}

	@Override
	public void updateShort(String columnLabel, short x) throws SQLException {
		// TODO Auto-generated method stub

	}

	@Override
	public void updateInt(String columnLabel, int x) throws SQLException {
		// TODO Auto-generated method stub

	}

	@Override
	public void updateLong(String columnLabel, long x) throws SQLException {
		// TODO Auto-generated method stub

	}

	@Override
	public void updateFloat(String columnLabel, float x) throws SQLException {
		// TODO Auto-generated method stub

	}

	@Override
	public void updateDouble(String columnLabel, double x) throws SQLException {
		// TODO Auto-generated method stub

	}

	@Override
	public void updateBigDecimal(String columnLabel, BigDecimal x)
			throws SQLException {
		// TODO Auto-generated method stub

	}

	@Override
	public void updateString(String columnLabel, String x) throws SQLException {
		// TODO Auto-generated method stub

	}

	@Override
	public void updateBytes(String columnLabel, byte[] x) throws SQLException {
		// TODO Auto-generated method stub

	}

	@Override
	public void updateDate(String columnLabel, Date x) throws SQLException {
		// TODO Auto-generated method stub

	}

	@Override
	public void updateTime(String columnLabel, Time x) throws SQLException {
		// TODO Auto-generated method stub

	}

	@Override
	public void updateTimestamp(String columnLabel, Timestamp x)
			throws SQLException {
		// TODO Auto-generated method stub

	}

	@Override
	public void updateAsciiStream(String columnLabel, InputStream x, int length)
			throws SQLException {
		// TODO Auto-generated method stub

	}

	@Override
	public void updateBinaryStream(String columnLabel, InputStream x, int length)
			throws SQLException {
		// TODO Auto-generated method stub

	}

	@Override
	public void updateCharacterStream(String columnLabel, Reader reader,
			int length) throws SQLException {
		// TODO Auto-generated method stub

	}

	@Override
	public void updateObject(String columnLabel, Object x, int scaleOrLength)
			throws SQLException {
		// TODO Auto-generated method stub

	}

	@Override
	public void updateObject(String columnLabel, Object x) throws SQLException {
		// TODO Auto-generated method stub

	}

	@Override
	public void insertRow() throws SQLException {
		// TODO Auto-generated method stub

	}

	@Override
	public void updateRow() throws SQLException {
		// TODO Auto-generated method stub

	}

	@Override
	public void deleteRow() throws SQLException {
		// TODO Auto-generated method stub

	}

	@Override
	public void refreshRow() throws SQLException {
		// TODO Auto-generated method stub

	}

	@Override
	public void cancelRowUpdates() throws SQLException {
		// TODO Auto-generated method stub

	}

	@Override
	public void moveToInsertRow() throws SQLException {
		// TODO Auto-generated method stub

	}

	@Override
	public void moveToCurrentRow() throws SQLException {
		// TODO Auto-generated method stub

	}

	@Override
	public Statement getStatement() throws SQLException {
		return stat;
	}

	@Override
	public Object getObject(int columnIndex, Map<String, Class<?>> map)
			throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Ref getRef(int columnIndex) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Blob getBlob(int columnIndex) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Clob getClob(int columnIndex) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Array getArray(int columnIndex) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object getObject(String columnLabel, Map<String, Class<?>> map)
			throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Ref getRef(String columnLabel) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Blob getBlob(String columnLabel) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Clob getClob(String columnLabel) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Array getArray(String columnLabel) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Date getDate(int columnIndex, Calendar cal) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Date getDate(String columnLabel, Calendar cal) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Time getTime(int columnIndex, Calendar cal) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Time getTime(String columnLabel, Calendar cal) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Timestamp getTimestamp(int columnIndex, Calendar cal)
			throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Timestamp getTimestamp(String columnLabel, Calendar cal)
			throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public URL getURL(int columnIndex) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public URL getURL(String columnLabel) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void updateRef(int columnIndex, Ref x) throws SQLException {
		// TODO Auto-generated method stub

	}

	@Override
	public void updateRef(String columnLabel, Ref x) throws SQLException {
		// TODO Auto-generated method stub

	}

	@Override
	public void updateBlob(int columnIndex, Blob x) throws SQLException {
		// TODO Auto-generated method stub

	}

	@Override
	public void updateBlob(String columnLabel, Blob x) throws SQLException {
		// TODO Auto-generated method stub

	}

	@Override
	public void updateClob(int columnIndex, Clob x) throws SQLException {
		// TODO Auto-generated method stub

	}

	@Override
	public void updateClob(String columnLabel, Clob x) throws SQLException {
		// TODO Auto-generated method stub

	}

	@Override
	public void updateArray(int columnIndex, Array x) throws SQLException {
		// TODO Auto-generated method stub

	}

	@Override
	public void updateArray(String columnLabel, Array x) throws SQLException {
		// TODO Auto-generated method stub

	}

	@Override
	public RowId getRowId(int columnIndex) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public RowId getRowId(String columnLabel) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void updateRowId(int columnIndex, RowId x) throws SQLException {
		// TODO Auto-generated method stub

	}

	@Override
	public void updateRowId(String columnLabel, RowId x) throws SQLException {
		// TODO Auto-generated method stub

	}

	@Override
	public int getHoldability() throws SQLException {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public boolean isClosed() throws SQLException {
		return dc == null;
	}

	@Override
	public void updateNString(int columnIndex, String nString)
			throws SQLException {
		// TODO Auto-generated method stub

	}

	@Override
	public void updateNString(String columnLabel, String nString)
			throws SQLException {
		// TODO Auto-generated method stub

	}

	@Override
	public void updateNClob(int columnIndex, NClob nClob) throws SQLException {
		// TODO Auto-generated method stub

	}

	@Override
	public void updateNClob(String columnLabel, NClob nClob) throws SQLException {
		// TODO Auto-generated method stub

	}

	@Override
	public NClob getNClob(int columnIndex) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public NClob getNClob(String columnLabel) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public SQLXML getSQLXML(int columnIndex) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public SQLXML getSQLXML(String columnLabel) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void updateSQLXML(int columnIndex, SQLXML xmlObject)
			throws SQLException {
		// TODO Auto-generated method stub

	}

	@Override
	public void updateSQLXML(String columnLabel, SQLXML xmlObject)
			throws SQLException {
		// TODO Auto-generated method stub

	}

	@Override
	public String getNString(int columnIndex) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getNString(String columnLabel) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Reader getNCharacterStream(int columnIndex) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Reader getNCharacterStream(String columnLabel) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void updateNCharacterStream(int columnIndex, Reader x, long length)
			throws SQLException {
		// TODO Auto-generated method stub

	}

	@Override
	public void updateNCharacterStream(String columnLabel, Reader reader,
			long length) throws SQLException {
		// TODO Auto-generated method stub

	}

	@Override
	public void updateAsciiStream(int columnIndex, InputStream x, long length)
			throws SQLException {
		// TODO Auto-generated method stub

	}

	@Override
	public void updateBinaryStream(int columnIndex, InputStream x, long length)
			throws SQLException {
		// TODO Auto-generated method stub

	}

	@Override
	public void updateCharacterStream(int columnIndex, Reader x, long length)
			throws SQLException {
		// TODO Auto-generated method stub

	}

	@Override
	public void updateAsciiStream(String columnLabel, InputStream x, long length)
			throws SQLException {
		// TODO Auto-generated method stub

	}

	@Override
	public void updateBinaryStream(String columnLabel, InputStream x, long length)
			throws SQLException {
		// TODO Auto-generated method stub

	}

	@Override
	public void updateCharacterStream(String columnLabel, Reader reader,
			long length) throws SQLException {
		// TODO Auto-generated method stub

	}

	@Override
	public void updateBlob(int columnIndex, InputStream inputStream, long length)
			throws SQLException {
		// TODO Auto-generated method stub

	}

	@Override
	public void updateBlob(String columnLabel, InputStream inputStream,
			long length) throws SQLException {
		// TODO Auto-generated method stub

	}

	@Override
	public void updateClob(int columnIndex, Reader reader, long length)
			throws SQLException {
		// TODO Auto-generated method stub

	}

	@Override
	public void updateClob(String columnLabel, Reader reader, long length)
			throws SQLException {
		// TODO Auto-generated method stub

	}

	@Override
	public void updateNClob(int columnIndex, Reader reader, long length)
			throws SQLException {
		// TODO Auto-generated method stub

	}

	@Override
	public void updateNClob(String columnLabel, Reader reader, long length)
			throws SQLException {
		// TODO Auto-generated method stub

	}

	@Override
	public void updateNCharacterStream(int columnIndex, Reader x)
			throws SQLException {
		// TODO Auto-generated method stub

	}

	@Override
	public void updateNCharacterStream(String columnLabel, Reader reader)
			throws SQLException {
		// TODO Auto-generated method stub

	}

	@Override
	public void updateAsciiStream(int columnIndex, InputStream x)
			throws SQLException {
		// TODO Auto-generated method stub

	}

	@Override
	public void updateBinaryStream(int columnIndex, InputStream x)
			throws SQLException {
		// TODO Auto-generated method stub

	}

	@Override
	public void updateCharacterStream(int columnIndex, Reader x)
			throws SQLException {
		// TODO Auto-generated method stub

	}

	@Override
	public void updateAsciiStream(String columnLabel, InputStream x)
			throws SQLException {
		// TODO Auto-generated method stub

	}

	@Override
	public void updateBinaryStream(String columnLabel, InputStream x)
			throws SQLException {
		// TODO Auto-generated method stub

	}

	@Override
	public void updateCharacterStream(String columnLabel, Reader reader)
			throws SQLException {
		// TODO Auto-generated method stub

	}

	@Override
	public void updateBlob(int columnIndex, InputStream inputStream)
			throws SQLException {
		// TODO Auto-generated method stub

	}

	@Override
	public void updateBlob(String columnLabel, InputStream inputStream)
			throws SQLException {
		// TODO Auto-generated method stub

	}

	@Override
	public void updateClob(int columnIndex, Reader reader) throws SQLException {
		// TODO Auto-generated method stub

	}

	@Override
	public void updateClob(String columnLabel, Reader reader) throws SQLException {
		// TODO Auto-generated method stub

	}

	@Override
	public void updateNClob(int columnIndex, Reader reader) throws SQLException {
		// TODO Auto-generated method stub

	}

	@Override
	public void updateNClob(String columnLabel, Reader reader)
			throws SQLException {
		// TODO Auto-generated method stub

	}

	@Override
	public <T> T getObject(int columnIndex, Class<T> type) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <T> T getObject(String columnLabel, Class<T> type) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

}
