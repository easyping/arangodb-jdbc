/*
 * Copyright (C) 2015 Hans-Christian Braun
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.hcbraun.arangodb.jdbc;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import com.arangodb.entity.BaseDocument;
import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;

public class JdbcDocument extends BaseDocument {

	private static final long serialVersionUID = 1L;
	
	public static class JdbcDocumentDeserializer implements
			JsonDeserializer<JdbcDocument> {

		@Override
		public JdbcDocument deserialize(JsonElement json, Type typeOfT,
				JsonDeserializationContext context) throws JsonParseException {

			JdbcDocument doc = new JdbcDocument();
			
			if (json.isJsonNull() || json.isJsonPrimitive() || json.isJsonArray())
				return doc;

			doc.setProperties(deserializeJsonObject(json.getAsJsonObject()));

			return doc;
		}

		/**
		 * desirializes any jsonElement
		 *
		 * @param jsonElement
		 * @return a object
		 */
		public static Object deserializeJsonElement(JsonElement jsonElement) {
			if (jsonElement.getClass() == JsonPrimitive.class) {
				return deserializeJsonPrimitive((JsonPrimitive) jsonElement);
			} else if (jsonElement.getClass() == JsonArray.class) {
				return deserializeJsonArray((JsonArray) jsonElement);
			} else if (jsonElement.getClass() == JsonObject.class) {
				return deserializeJsonObject((JsonObject) jsonElement);
			}
			return null;
		}

		/**
		 * desirializes a JsonObject into a Map<String, Object>
		 *
		 * @param jsonObject
		 *          a jsonObject
		 * @return the deserialized jsonObject
		 */
		private static Map<String, Object> deserializeJsonObject(
				JsonObject jsonObject) {
			Map<String, Object> result = new HashMap<String, Object>();
			Set<Entry<String, JsonElement>> entrySet = jsonObject.entrySet();
			for (Map.Entry<String, JsonElement> entry : entrySet) {
				result.put(entry.getKey(),
						deserializeJsonElement(jsonObject.get(entry.getKey())));
			}
			return result;
		}

		private static List<Object> deserializeJsonArray(JsonArray jsonArray) {
			List<Object> tmpObjectList = new ArrayList<Object>();
			Iterator<JsonElement> iterator = jsonArray.iterator();
			while (iterator.hasNext()) {
				tmpObjectList.add(deserializeJsonElement(iterator.next()));
			}
			return tmpObjectList;
		}

		/**
		 * deserializes a jsonPrimitiv into the equivalent java primitive
		 *
		 * @param jsonPrimitive
		 * @return null|String|Double|Boolean
		 */
		private static Object deserializeJsonPrimitive(JsonPrimitive jsonPrimitive) {
			if (jsonPrimitive.isBoolean()) {
				return jsonPrimitive.getAsBoolean();
			} else if (jsonPrimitive.isNumber()) {
				return jsonPrimitive.getAsDouble();
			} else if (jsonPrimitive.isString()) {
				return jsonPrimitive.getAsString();
			}
			return null;
		}

	}

}
