package de.hcbraun.arangodb.jdbc;

import static org.junit.Assert.assertEquals;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;
import java.sql.Types;

import org.junit.BeforeClass;
import org.junit.Test;

import com.arangodb.ArangoConfigure;
import com.arangodb.ArangoDriver;
import com.arangodb.ArangoHost;
import com.arangodb.util.MapBuilder;

public class JdbcResultSetTest {

	private final static String DB_HOST = "arangodb-test";
	private final static int DB_PORT = 8529;
	private final static String DB_JDBCTEST = "jdbctest";
	private final static String COL_JDBCTEST = "colExample";

	@BeforeClass
	public static void setup() {
		// Initialize configure
		ArangoConfigure configure = new ArangoConfigure();
		configure.setArangoHost(new ArangoHost(DB_HOST, DB_PORT));
		configure.init();

		// Create Driver (this instance is thread-safe)
		// If you use a multi database, you need create each instance.
		ArangoDriver driverSys = new ArangoDriver(configure);
		ArangoDriver driverTst = new ArangoDriver(configure, DB_JDBCTEST);

		try {
			try {
				driverSys.deleteDatabase(DB_JDBCTEST);
			} catch (Exception e) {
			}
			driverSys.createDatabase(DB_JDBCTEST, null);

			driverTst.createCollection(COL_JDBCTEST);
			driverTst.createDocument(COL_JDBCTEST,
					new MapBuilder().put("name", "Dev-Test").put("nr", "1").get(), false,
					false);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			configure.shutdown();
		}
	}

	@Test
	public void testName() {
		try {
			Class.forName("de.hcbraun.arangodb.jdbc.ArangoDBDriver").newInstance();
			Connection conT = DriverManager.getConnection("jdbc:hcbraun:arangodb:"
					+ DB_HOST + ":" + DB_PORT + "/" + DB_JDBCTEST, "", "");
			Statement statS = conT.createStatement();

			ResultSet rs = statS.executeQuery("for x in " + COL_JDBCTEST
					+ " return {test: x}");
			if (rs != null) {
				if (rs.next())
					assertEquals("Name: ", "Dev-Test", rs.getString("test.name"));
				rs.close();
			}

			conT.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void testColType() {
		try {
			Class.forName("de.hcbraun.arangodb.jdbc.ArangoDBDriver").newInstance();
			Connection conT = DriverManager.getConnection("jdbc:hcbraun:arangodb:"
					+ DB_HOST + ":" + DB_PORT + "/" + DB_JDBCTEST, "", "");
			Statement statS = conT.createStatement();

			ResultSet rs = statS.executeQuery("for x in " + COL_JDBCTEST
					+ " return {test: x} // cols: test.name:s, test.nr:i");
			if (rs != null) {
				ResultSetMetaData rsmd = rs.getMetaData();
				assertEquals("Col-Anz: ", rsmd.getColumnCount(), 2);

				assertEquals("Col-2-Type: ", rsmd.getColumnType(2), Types.INTEGER);
				rs.close();
			}

			conT.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
