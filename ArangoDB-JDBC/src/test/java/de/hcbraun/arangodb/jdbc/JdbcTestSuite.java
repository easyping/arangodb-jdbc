package de.hcbraun.arangodb.jdbc;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({JdbcResultSetTest.class})
public class JdbcTestSuite {

}
