# ArangoDB-JDBC #

Das Ziel dieser Schnittstelle ist, dass innerhalb von [JasperReport](http://community.jaspersoft.com/) auf die Daten von [ArangoDB](https://www.arangodb.com/) zugegriffen werden kann.

Diese Schnittstelle kann im Prinzip auch in jeder anderen Programm, dass JDBC verwendet, benützt werden, wobei die JDBC-Schnittstelle nicht komplett ausprogrammiert ist.

Der eigentliche SQL-Befehl muss im Syntax von AQL-Befehl erfolgen, da dieser so an die ArangoDB weitergleitet wird.


Zum Beispiel:

for x in Person return x

Als Ergebnis würde folgende JSON-Daten zurück geliefert werden:

[{name: "Mustermann", vorname: "Max", adresse: {str: "Musterstr. 1", plz: "54321", ort: "Musterstadt"}, mail: "max.mustermann@online.de", geburtstag: "1900-1-1"}]

Aus diesem Ergebnis stehen folgende Felder zu Verfügung, die auch so in JasperReport als Felder verwendet werden

name, vorname, adresse.str, adresse.plz, adresse.ort, mail, geburtstag


Da in einem JSON-Datenstrom nur die Typen String, Double und Boolean vorkommen können, kann beim AQL-Befehl innerhalb von Kommentare die Spalten inkl. Datentyp aufgeführt werden.

Zum Beispiel:

for x in Person return x // cols: name:s,adresse.str:s,adresse.ort:s,geburtstag:dt

Hier stehen dann in JasperReport nur folgende Felder zu Verfügung:

name, adresse.str, adresse.ort, geburtstag

wobei die ersten drei Felder String-Felder sind und das Feld Geburtstag ein Date-Feld ist.


Folgende Datentypen stehen zu Verfügung (Langversion / Kurzversion)

String / s
Integer / i
Boolean / b
Double / d
Float / f
BigDecimal / bd
Date / dt
Time / t
Timestamp / ts


Zum einbinden der ArangoDB-JDBC-Schnittstelle am besten die ArangoDB-JDBC-<Version>-jar-with-dependencies.jar verwenden. Diese kann unter Quellecode - AranogDB-JDBC/target gefunden werden.